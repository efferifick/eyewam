browser.runtime.onMessage.addListener(function(m, sender, sendResponse) {
      var link = document.createElement('a');
      link.href = m;
      link.download = 'screenshot.png';
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
});
