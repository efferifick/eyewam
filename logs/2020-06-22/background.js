let portFromCS;

function onCaptured(imageUri) {
  portFromCS.postMessage(imageUri);
}

function onError(error) {
  console.log(`Error: ${error}`);
}


function connected(p) {
  portFromCS = p;
  browser.browserAction.onClicked.addListener(function() {
    var capturing = browser.tabs.captureVisibleTab();
    capturing.then(onCaptured, onError);
  });
}

browser.runtime.onConnect.addListener(connected);                            
